package common

const (
	// DefaultUserAgent parts of the User-Agent are taken from an actual request coming from a Boxee Box.
	DefaultUserAgent = "boxee-server/0.0.1 (Linux 2.6.28 intel.ce4100 dlink.dsm380 i686; en-US; beta) boxee/1.2.3.20490-a104422"
)
