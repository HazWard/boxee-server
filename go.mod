module boxee-server

require (
	github.com/gorilla/mux v1.8.1
	github.com/rs/xid v1.5.0
	github.com/urfave/cli/v2 v2.26.0
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
)

go 1.21
