package boxee

import (
	"fmt"
	"log/slog"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
)

const pingResponseTemplate = `<?xml version='1.0' encoding='ISO-8859-1' ?>
<ping><cmds ping_version='9'></cmds><timestamp utc='%d'/></ping>`

// Ping verifies if network connection is still working.
// The response from the request is a timestamp.
func Ping() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var attrs []any
		attrs = append(attrs, slog.String("remote_addr", r.RemoteAddr))
		vars := mux.Vars(r)
		ping, _ := vars["pingid"]
		ping = strings.TrimSpace(ping)
		if ping != "" {
			attrs = append(attrs, slog.String("pingid", ping))
		}

		slog.Debug("Ping from client", attrs...)
		timestamp := time.Now().UnixNano() / int64(time.Millisecond)
		_, err := w.Write([]byte(fmt.Sprintf(pingResponseTemplate, timestamp)))
		if err != nil {
			slog.Error("Error while responding to ping", slog.Any("error", err))
		}
	}
}
