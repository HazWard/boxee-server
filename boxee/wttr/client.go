package wttr

import (
	"boxee-server/common"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

const defaultURL = "https://wttr.in/?format=j1"

type Client struct {
	c *http.Client
}

func NewWttrClient(client *http.Client) *Client {
	return &Client{
		c: client,
	}
}

func (c *Client) FetchCurrentWeatherConditions(ctx context.Context) (*Response, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, defaultURL, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to build request to get weather: %w", err)
	}

	req.Header.Set("User-Agent", common.DefaultUserAgent)

	resp, err := c.c.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error occured during request: %w", err)
	}

	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body: %w", err)
	}

	if resp.StatusCode > 399 {
		return nil, fmt.Errorf("unsuccessful status code %d from request: %s", resp.StatusCode, string(body))
	}

	wttrResponse := &Response{}
	if err := json.Unmarshal(body, &wttrResponse); err != nil {
		return nil, fmt.Errorf("failed to parse response as JSON: %w\n%s", err, string(body))
	}

	return wttrResponse, nil
}
