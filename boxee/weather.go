package boxee

import (
	"boxee-server/boxee/wttr"
	"context"
	"encoding/json"
	"fmt"
	"log/slog"
	"net/http"
)

type weatherResponse struct {
	Temperature string `json:"temperature"`
	RealFeel    string `json:"realfeel"`
	WeatherIcon string `json:"weathericon"`
}

// FetchWeatherData get the current weather from wttr.in.
// This provider takes care of getting the weather based
// on the caller's location from the IP so there's no need
// to send the location
func FetchWeatherData(httpClient *http.Client) (http.HandlerFunc, error) {
	weatherClient := wttr.NewWttrClient(httpClient)
	slog.Info("Fetching weather conditions...")
	resp, err := weatherClient.FetchCurrentWeatherConditions(context.Background())
	if err != nil {
		return nil, fmt.Errorf("failed to get weather conditions: %w", err)
	}

	current, ok := resp.Current()
	if !ok {
		slog.Error("No weather conditions found, faking results", slog.Any("response", resp))
		current = wttr.CurrentCondition{
			TempC:      "10",
			FeelsLikeC: "10",
		}
	}

	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		if err := json.NewEncoder(w).Encode(weatherResponse{
			Temperature: current.TempC,
			RealFeel:    current.FeelsLikeC,
			WeatherIcon: current.WeatherCode,
		}); err != nil {
			slog.Error("Error while writing weather conditions response", slog.Any("error", err), slog.Any("response", resp))
		}
	}, nil
}
