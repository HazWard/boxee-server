package boxee

import (
	"boxee-server/logging"
	"fmt"
	"log/slog"
	"net/http"
)

// HandleBoxeeTraffic is the default way of handling traffic going to
// *.boxee.tv URLs. This simply dumps the callers requests into the response
// making sure that the request receives a 200 OK.
//
// More specific handlers are create to return the appropriate data.
// Note: This is mainly used for debugging.
func HandleBoxeeTraffic() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		reqAttr := logging.RequestData(r)
		var attrValues []any
		for k, v := range reqAttr {
			attrValues = append(attrValues, slog.Any(k, v))
		}

		slog.Debug("Handling boxee.tv request", slog.Group("request", attrValues...))
		_, err := fmt.Fprintf(w, "Handling boxee.tv request\nRequest: %s\n", reqAttr)
		if err != nil {
			slog.Error("Failed to write response for fallback traffic")
		}
	}
}
