package boxee

import (
	"fmt"
	"log/slog"
	"net/http"
)

const loginResponseTemplate = `<?xml version="1.0" encoding="UTF-8" ?>
<object type="user" id="boxeeuser">
<name>Boxee User</name>
<short_name>Boxee User</short_name>
<thumb></thumb>
<thumb_small></thumb_small>
<user_id>boxeeuser</user_id>
<user_display_name>Boxee User</user_display_name>
<user_first_name></user_first_name>
<user_last_name></user_last_name>
<country>US</country>
<show_movie_library>1</show_movie_library>
</object>
`

func Login() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		slog.Debug("Faking a login")
		_, err := fmt.Fprint(w, loginResponseTemplate)
		if err != nil {
			slog.Error("Failed to write response for fake login", slog.Any("error", err))
		}
	}
}
