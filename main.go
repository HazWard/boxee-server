package main

import (
	"boxee-server/server"
	"context"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/urfave/cli/v2"
)

const defaultPort = 7777

func main() {
	code := 0
	defer func() {
		os.Exit(code)
	}()

	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer stop()
	logLevel := &slog.LevelVar{}
	log := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{
		AddSource: true,
		Level:     logLevel,
	}))
	logLevel.Set(slog.LevelInfo)
	slog.SetDefault(log)

	host := "0.0.0.0"
	client := &http.Client{
		Timeout: 10 * time.Second,
	}

	app := &cli.App{
		Name:      "server",
		Usage:     "A replacement Boxee server",
		Writer:    os.Stdout,
		ErrWriter: os.Stderr,
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:  "debug",
				Value: false,
			},
			&cli.UintFlag{
				Name:    "port",
				Value:   defaultPort,
				Aliases: []string{"p"},
				EnvVars: []string{"BOXEE_SERVER_PORT"},
			},
		},
		Before: func(cli *cli.Context) error {
			if cli.Bool("debug") {
				logLevel.Set(slog.LevelDebug)
			}

			return nil
		},
		Commands: []*cli.Command{
			{
				Name:  "proxy",
				Usage: "Start the server in proxy mode (usable as an HTTP proxy).",
				Action: func(cli *cli.Context) error {
					return server.RunProxyMode(host, cli.Uint("port"), client)
				},
			},
			{
				Name:  "dns",
				Usage: "Start the server in DNS mode (resolving DNS queries).",
				Action: func(cli *cli.Context) error {
					return server.DNSMode(host, cli.Uint("port"), client)
				},
			},
		},
	}

	if err := app.RunContext(ctx, os.Args); err != nil {
		log.Error("Exiting server with error.", slog.Any("error", err))
		code = 1
		return
	}
}
