FROM golang:1.21-alpine AS builder
WORKDIR /src
RUN apk add --no-cache git
ADD go.mod /src/go.mod
ADD go.sum /src/go.sum
RUN go mod download
ADD . /src
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o boxee-server

FROM alpine:edge

ENV WEBPROC_VERSION 0.2.2
ENV WEBPROC_URL https://github.com/jpillora/webproc/releases/download/$WEBPROC_VERSION/webproc_linux_amd64.gz
ENV BOXEE_SERVER_PORT=80
ENV EXTERNAL_IP=""

# Fetch dnsmasq and webproc binary
RUN apk update \
	&& apk --no-cache add dnsmasq \
	&& apk add --no-cache --virtual .build-deps curl \
	&& curl -sL $WEBPROC_URL | gzip -d - > /usr/local/bin/webproc \
	&& chmod +x /usr/local/bin/webproc \
	&& apk del .build-deps

# Configure dnsmasq
RUN mkdir -p /etc/default/
RUN echo -e "ENABLED=1\nIGNORE_RESOLVCONF=yes" > /etc/default/dnsmasq

RUN apk add --no-cache gettext ca-certificates
ADD docker/start.sh /start.sh
ADD docker/dnsmasq.conf.template /etc/dnsmasq.conf.template
ADD docker/hosts.dnsmasq.template /etc/hosts.dnsmasq.template
COPY --from=builder /src/boxee-server /boxee-server
RUN chmod +x /start.sh

ENTRYPOINT ["/start.sh"]
