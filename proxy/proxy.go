package proxy

import (
	"io"
	"log/slog"
	"net/http"

	"github.com/rs/xid"
)

// HandleRequest receives the incoming requests and performs
// it on behalf of the caller without any modifications
func HandleRequest(client *http.Client) http.HandlerFunc {
	id := xid.New().String()
	log := slog.With("req_id", id)
	return func(w http.ResponseWriter, r *http.Request) {
		var err error
		defer func() {
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				log.Error("Error on proxy request", slog.Any("error", err))
				return
			}
		}()

		log.Info("Proxying request", slog.String("method", r.Method), slog.String("uri", r.RequestURI), slog.String("path", r.URL.Path))
		req, err := http.NewRequest(r.Method, r.RequestURI, r.Body)
		if err != nil {
			log.Error("Failed to build proxy request", slog.Any("error", err))
			return
		}

		for name, value := range r.Header {
			req.Header.Set(name, value[0])
		}
		resp, err := client.Do(req)
		if err != nil {
			return
		}
		_ = r.Body.Close()

		for k, v := range resp.Header {
			w.Header().Set(k, v[0])
		}
		w.WriteHeader(resp.StatusCode)
		_, err = io.Copy(w, resp.Body)
		defer resp.Body.Close()
		if err != nil {
			log.Error("Failed to copy data from proxied request", slog.Any("error", err))
		}
	}
}
