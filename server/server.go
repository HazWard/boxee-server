package server

import (
	"boxee-server/boxee"
	"boxee-server/proxy"
	"fmt"
	"log/slog"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func RunProxyMode(host string, port uint, client *http.Client) error {
	slog.Info("Starting server in Proxy mode")

	return runServer(host, port, func(router *mux.Router) error {
		router.Host("ping.boxee.tv").HandlerFunc(boxee.Ping())
		router.Host("{pingid:[0-9]+}.ping.boxee.tv").HandlerFunc(boxee.Ping())
		boxeeRouter := router.Host("{subdomain:[a-zA-Z]+}.boxee.tv").Subrouter()
		boxeeRouter.HandleFunc("/ping/dlink", boxee.Ping())
		boxeeRouter.HandleFunc("/api/login", boxee.Login())
		weatherFunc, err := boxee.FetchWeatherData(client)
		if err != nil {
			return err
		}
		boxeeRouter.HandleFunc("/location/weather", weatherFunc)
		boxeeRouter.PathPrefix("/").HandlerFunc(boxee.HandleBoxeeTraffic())
		router.PathPrefix("/").Handler(proxy.HandleRequest(client))

		return nil
	})
}

func DNSMode(host string, port uint, client *http.Client) error {
	slog.Info("Starting server in DNS mode")

	return runServer(host, port, func(router *mux.Router) error {
		// Handle ping.boxee.tv
		router.HandleFunc("/ping", boxee.Ping())
		router.PathPrefix("/ping").HandlerFunc(boxee.Ping())

		// Handle app.boxee.tv
		router.HandleFunc("/api/login", boxee.Login())
		router.HandleFunc("/app/api/login", boxee.Login())
		router.HandleFunc("/app/ping/dlink", boxee.Ping())
		weatherFunc, err := boxee.FetchWeatherData(client)
		if err != nil {
			return err
		}
		router.HandleFunc("/app/location/weather", weatherFunc)
		router.HandleFunc("/location/weather", weatherFunc)

		router.PathPrefix("/app").HandlerFunc(boxee.HandleBoxeeTraffic())

		// Handle res.boxee.tv
		router.PathPrefix("/res").HandlerFunc(boxee.HandleBoxeeTraffic())

		// Handle s3.boxee.tv
		router.PathPrefix("/s3").HandlerFunc(boxee.HandleBoxeeTraffic())

		// Fallback
		router.PathPrefix("/").HandlerFunc(boxee.HandleBoxeeTraffic())

		return nil
	})
}

func baseMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func(start time.Time) {
			err := recover()
			attrs := []any{
				slog.String("method", r.Method),
				slog.Duration("duration", time.Since(start)),
				slog.String("url", r.URL.String()),
			}
			if err != nil {
				attrs = append(attrs, slog.Any("error", err))
				slog.Error("Request completed", attrs...)
			} else {
				slog.Info("Request completed", attrs...)
			}
		}(time.Now())
		next.ServeHTTP(w, r)
	})
}

func runServer(host string, port uint, setupFunc func(*mux.Router) error) error {
	router := mux.NewRouter()
	router.Use(baseMiddleware)
	if err := setupFunc(router); err != nil {
		return fmt.Errorf("router setup failed: %w", err)
	}

	addr := fmt.Sprintf("%s:%d", host, port)
	srv := &http.Server{
		Addr:         addr,
		Handler:      router,
		ReadTimeout:  15 * time.Second,
		WriteTimeout: 15 * time.Second,
	}

	slog.Info("Boxee Server Listening", slog.String("addr", addr))
	return srv.ListenAndServe()
}
