package logging

import (
	"net/http"
	"strings"
)

func RequestData(req *http.Request) map[string]interface{} {
	headers := make(map[string]string)
	for k, v := range req.Header {
		headers[k] = strings.Join(v, "; ")
	}

	return map[string]interface{}{
		"method":      req.Method,
		"remote_addr": req.RemoteAddr,
		"url":         req.URL,
		"headers":     headers,
	}
}
